#include <iostream>

using namespace std;

extern "C" int GetValueFromAsm();

int main()
{
	cout << "Sup, ASM said " << GetValueFromAsm() << endl;
	return 0;
}