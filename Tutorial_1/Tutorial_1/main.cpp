#include <iostream>
#include <conio.h>

using namespace std;

extern "C" int GetValueFromASM();

int main()
{
	cout << "ASM, said " << GetValueFromASM() << endl;
	_getch();
	return 0;
}